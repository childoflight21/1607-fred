﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.DataAccess
{
  public class AdoData
  {
    private string dbConnect = @"data source=.\sqljuly2016;initial catalog=PokemonDB;user id=sa;password=12345;";

    public void GetAllPokemon_Disconnected()
    {
      DataSet ds = new DataSet();
      SqlDataAdapter da = new SqlDataAdapter();
      SqlCommand com = new SqlCommand();
      string query = "select * from character.character;";

      using (SqlConnection con = new SqlConnection(dbConnect))
      {
        com.Connection = con;
        com.CommandText = query;

        da.SelectCommand = com;
        da.Fill(ds);
      }

      foreach (DataRow item in ds.Tables[0].Rows)
      {
        Console.WriteLine(item["Fred-Raptor"]);
      }
    }

    public void GetAllPokemon_Connected()
    {
      SqlDataReader dr;
      SqlCommand com = new SqlCommand();
      string query = "select * from character.character;";

      using (SqlConnection con = new SqlConnection(dbConnect))
      {
        con.Open();
        com.Connection = con;
        com.CommandText = query;

        dr = com.ExecuteReader();

        while (dr.Read())
        {
          Console.WriteLine(dr["Name"]);
        }
      }
    }

    public bool InsertPokemon(string name, SqlDecimal height, SqlDecimal weight)
    {
      SqlDataAdapter da = new SqlDataAdapter();
      SqlCommand command = new SqlCommand();
      string query = "insert into character.character(name, height, weight, datemodified, active) values (@name, @height, @weight, @datetime,1)";
      SqlParameter paramName = new SqlParameter("name", name);
      SqlParameter paramHeight = new SqlParameter("height", height);
      SqlParameter paramWeight = new SqlParameter("weight", weight);
      SqlParameter paramDatetime = new SqlParameter("datetime", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
      int result;

      using (var con = new SqlConnection(dbConnect))
      {
        con.Open();
        command.Connection = con;
        command.CommandText = query;
        command.Parameters.Add(paramName);
        command.Parameters.Add(paramHeight);
        command.Parameters.Add(paramWeight);
        command.Parameters.Add(paramDatetime);

        result = command.ExecuteNonQuery();
      }

      //if (result > 0)
      //{
      //  return true;
      //}
      //else
      //{
      //  return false;
      //}

      //return result > 0 ? true : false;

      return result != 0;
    }
  }
}
