﻿using Pokemon.BusinessLogic;
using Pokemon.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.ConsoleClient
{
  class Program
  {
    static void Main(string[] args)
    {
      //GetPokemonAdoData();
      //GetPokemonEFData();
      //GetAllPokemonVitals();

      //InsertPokemonAdoData();
      //InsertPokemonEFData();
      //ChangePokemonEFData();
      //UpdatePokemonHeight();

      //GetPokemonData();

      //LazyLoader();

      WorkWithDTO();

      Console.ReadLine();
    }

    public static void GetPokemonAdoData()
    {
      AdoData ad = new AdoData();

      ad.GetAllPokemon_Disconnected();
      ad.GetAllPokemon_Connected();
    }

    public static void InsertPokemon()
    {
      var name = "thursday";
      SqlDecimal height = new SqlDecimal(3.16M);
      SqlDecimal weight = new SqlDecimal(10.23M);
      var ad = new AdoData();

      bool result = ad.InsertPokemon(name, height, weight);

      if (result)
      {
        Console.WriteLine("it worked");
      }
      else
      {
        Console.WriteLine("it failed");
      }
    }

    public static void GetPokemonEFData()
    {
      var ed = new EFData();

      ed.GetAllPokemon();
    }

    public static void InsertPokemonEFData()
    {
      var ed = new EFData();
      var c = new Character();

      c.Name = "Afternoon";
      c.Height = 6.02M;
      c.Weight = 101.25M;
      c.DateModified = DateTime.Now;
      c.DocumentGuid = Guid.NewGuid();
      c.Active = true;
      
      if (ed.InsertPokemon(c))
      {
        Console.WriteLine("ef rocks!");
      }
      else
      {
        Console.WriteLine("ef sucks!");
      }
    }

    public static void ChangePokemonEFData()
    {
      var ed = new EFData();
      var c = new Character();

      c.Name = "AfternoonNext";
      c.Height = 6.02M;
      c.Weight = 101.25M;
      c.DateModified = DateTime.Now;
      c.DocumentGuid = Guid.NewGuid();
      c.Active = true;

      if (ed.ChangePokemon(c, EntityState.Added))
      {
        Console.WriteLine("ef rocks!");
      }
      else
      {
        Console.WriteLine("ef sucks!");
      }
    }

    public static void GetAllPokemonVitals()
    {
      var data = new EFData();

      foreach (var item in data.GetAllPokemonVitals())
      {
        Console.WriteLine(item.name);
      }
    }

    public static void UpdatePokemonHeight()
    {
      var data = new EFData();

      data.UpdatePokemonHeight(1, 1.22M);
    }

    public static void GetPokemonData()
    {
      var data = new EFData();

      Console.WriteLine(EFData.GetPokemonData(1));
    }

    public static void LazyLoader()
    {
      var ef = new EFData();

      foreach (var item in ef.LazyLoading())
      {
        Console.WriteLine(item.Name);
      }
    }

    public static void WorkWithDTO()
    {
      var c = new CharacterDTO();

      c.CharacterName = "a";

      Console.WriteLine(c.CharacterName);
    }
  }
}
