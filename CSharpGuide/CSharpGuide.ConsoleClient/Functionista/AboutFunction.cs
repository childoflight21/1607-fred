﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient.Functionista
{
  class AboutFunction
  {
    public object Compute(Func<object, object, object> o, object a, object b)
    {
      Console.WriteLine(DateTime.Now.Ticks); 
      var r = o(a, b);
      Console.WriteLine(DateTime.Now.Ticks);

      return r;
    }
  }
}
