﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static CSharpGuide.ConsoleClient.Observable.Publisher;

namespace CSharpGuide.ConsoleClient.Observable
{
  class Publisher
  {
    public event BroadcastHandler OnAir;
    public delegate void BroadcastHandler();

    public void Broadcast()
    {
      var count = 1;

      while (count < 6)
      {
        Console.WriteLine("event fired");
        Thread.Sleep(2000);

        if (OnAir != null)
        {
          OnAir();
        }
        
        count += 1;
      }
    }
  }

  class SubscriberA
  {
    public void Listen(Publisher pub)
    {
      pub.OnAir += ListeningToRadio;
    }

    private void ListeningToRadio()
    {
      Console.WriteLine("i heard it");
    }
  }

  class SubscriberB
  {
    public void Listen(Publisher pub)
    {
      pub.OnAir += ListeningToRadio;
    }

    private void ListeningToRadio()
    {
      Console.WriteLine("enough talking");
    }
  }
}
