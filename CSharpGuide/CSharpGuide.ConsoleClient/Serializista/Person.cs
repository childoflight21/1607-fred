﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CSharpGuide.ConsoleClient.Serializista
{
  public class Person
  {
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public List<Address> Addresses { get; set; }
  }
}
