﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient.ThreadLine
{
  public class SeeYouLater
  {
    private void ConsoleOutput(string stuff, int count)
    {
      for (int i = 0; i < count; i += 1)
      {
        Console.Write(stuff);
      }
    }

    public void WorkWithThread()
    {
      var t1 = new Thread(() => { ConsoleOutput("A", 100); });
      var t2 = new Thread(() => { ConsoleOutput("B", 100); });
      var t3 = new Thread(() => { ConsoleOutput("C", 100); });

      ConsoleOutput("D", 100);

      t1.Start();
      t2.Start();
      t3.Start();

      t1.Join();
      t2.Join();
      t3.Join();
    }

    public void WorkWithTask()
    {
      var t1 = new Task(() => { ConsoleOutput("A", 10000); });
      var t2 = new Task(() => { ConsoleOutput("B", 10000); });
      var t3 = new Task(() => { ConsoleOutput("C", 10000); });

      t1.Start();
      t2.Start();
      t3.Start();

      ConsoleOutput("D", 10000);

      Task.WaitAll(new Task[] { t1, t2, t3 }, 1);
    }

    public Task WorkWithAsync()
    {
      return Task.Run(() => { Thread.Sleep(3000); Console.WriteLine("Task is Calling"); });
    }
  }
}
