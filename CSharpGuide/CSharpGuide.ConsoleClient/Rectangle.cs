﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient
{
  public class Rectangle : Shape
  {
    public double Height { get; set; }
    public double Length { get; set; }
    public virtual double Width { get; set; }

    public Rectangle() : base(4)
    {

    }

    public override double Area()
    {
      return Length * Width;
    }

    public override double Volume()
    {
      return Area() * Height;
    }
  }
}
