﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CSharpGuide.ConsoleClient.Serializista
{
  public class Contact
  {
    public void Add()
    {
      Console.Write("enter your name: ");
      var name = Console.ReadLine();

      Console.WriteLine();

      Console.Write("enter your addresses: ");
      var addresses = Console.ReadLine().Split(';');

      var p = new Person();

      p.Firstname = name.Split()[0];
      p.Lastname = name.Split()[1];
      p.Addresses = new List<Address>();

      foreach (var item in addresses.ToList())
      {
        p.Addresses.Add(new Address()
        {
          City = item.Split(',')[0]
          ,State = item.Split(',')[1]
        });
      }

      WriteToFile(p);
    }

    public List<Person> Get()
    {
      var path = @"c:\revature\contact.txt";
      var people = new List<Person>();
      var linesOfText = ReadFromFile(path);

      foreach (var item in linesOfText)
      {
        var p = new Person();
        var record = item.Split('-');
        var name = record[0];
        var addresses = record[1];

        p.Firstname = name.Split()[0];
        p.Lastname = name.Split()[1];
        people.Add(p);
      }

      return people;
    }

    public Person GetXml()
    {
      var path = @"c:\revature\contact.xml";
      return ReadFromFileXml(path);
    }

    private void WriteToFile(Person p)
    {
      var path = @"c:\revature\contact.txt";

      using (var file = File.AppendText(path))
      {
        file.Write(string.Format("{0} {1} - ", p.Firstname, p.Lastname));

        foreach (var item in p.Addresses)
        {
          file.Write(string.Format("{0},{1}; ", item.City, item.State));
        }

        file.WriteLine();
      }
    }

    private void WriteToFileXml(Person p)
    {
      var path = @"c:\revature\contact.xml";
      var xml = new XmlSerializer(typeof(Person));
      var writer = new StreamWriter(path);

      xml.Serialize(writer, p);
      writer.Close();
    }

    private List<string> ReadFromFile(string path)
    {
      var s = new List<string>();
      var lines = File.ReadAllLines(path);

      s = lines.ToList();

      return s;
    }

    private Person ReadFromFileXml(string path)
    {
      var xml = new XmlSerializer(typeof(Person));
      var reader = new StreamReader(path);
      Person p;

      p = xml.Deserialize(reader) as Person;

      return p;
    }

    private void WriteCustomXml()
    {
      var xmlDoc = new XmlDocument();
      var name = xmlDoc.CreateElement("firstname");
      name.InnerText = "fred";

      xmlDoc.AppendChild(name);
    }
  }
}
