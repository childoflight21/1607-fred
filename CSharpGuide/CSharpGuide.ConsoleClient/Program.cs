﻿using CSharpGuide.ConsoleClient.Functionista;
using CSharpGuide.ConsoleClient.Observable;
using CSharpGuide.ConsoleClient.Serializista;
using CSharpGuide.ConsoleClient.ThreadLine;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace CSharpGuide.ConsoleClient
{
  class Program
  {
    private static ILog log = LogManager.GetLogger("logaround");

    static void Main(string[] args)
    {
      //PlayWithEnum();

      //LetsChangeString();

      //PlayWithThread();

      //PlayWithAsync();
      //log.Debug("using main method");

      //Console.WriteLine("End of Main");

      PlayWithSerialization();

      Console.ReadLine();
    }

    public static void PlayWithShape()
    {
      IForm s = new Rectangle();
      Square sq = s as Square;
      Rectangle r = (Rectangle)s;
      Sphere sp = s as Sphere;
    }

    public static void WhatIsTheShape(Shape s)
    {
      Console.WriteLine(s.Name);
    }

    public static void WhatisYourPerimeter(IForm i)
    {
      Console.WriteLine(i.Perimeter());
    }

    public static void PlayWithStruct()
    {
      var p = new Point(3, 4);
    }

    public static void PlayWithEnum()
    {
      Console.WriteLine(StateCode.MDA);
    }

    public static void LetsChangeString()
    {
      var s = "fred";

      Console.WriteLine(s.Capitalize());
    }

    public static void PlayWithFunction()
    {
      var af = new AboutFunction();

      //af.Add(1, 2);
      //af.Compute((a, b) => { return a + b + (a-b); }, 1, 2);
      //af.Compute(NewAdd, 1, 2);

      //af.Compute(AnotherAdd, 3, 4);
      NewAdd(1, 2);
    }

    public static double NewAdd(double a, double b)
    {
      AddString("hello", "banana");
      return a + b;

    }

    public static string AnotherAdd(double a, double b)
    {
      return string.Format("{0} {1} {2}", a, b, AddString("a", "b"));
    }

    public static string AddString(string a, string b)
    {
      return string.Format("{0} {1}", a, b);
    }

    public static void PlayWithEvent()
    {
      var p = new Publisher();
      var s1 = new SubscriberA();
      var s2 = new SubscriberB();

      s1.Listen(p);
      s2.Listen(p);

      p.Broadcast();
    }

    public static void PlayWithThread()
    {
      var t = new SeeYouLater();

      //t.WorkWithThread();
      t.WorkWithTask();
    }

    public static async void PlayWithAsync()
    {

      var t = new SeeYouLater();

      await t.WorkWithAsync();
    }

    public static void PlayWithSerialization()
    {
      var c = new Contact();

      //c.Add();
      //Console.WriteLine();

      //Thread.Sleep(3000);

      //var p = c.Get();
      //Console.WriteLine("you have entered this contact {0} {1}", p[0].Firstname, p[0].Lastname);

      var p = c.GetXml();
      Console.WriteLine("you have entered this contact {0} {1}", p.Firstname, p.Lastname);
    }
  }
}
