﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient
{
  public abstract class Shape : IForm
  {
    #region backing fields

    private int _NumSides = default(int);
    private string _Name = default(string);

    #endregion

    #region properties

    public int NumSides
    {
      get;
    }

    public string Name
    {
      get
      {
        return _Name;
      }

      set
      {
        if (string.IsNullOrWhiteSpace(value))
        {
          return;
        }

        if (Regex.IsMatch(value, @"^[a-z]+$"))
        {
          _Name = value;
        }
      }
    }

    #endregion

    #region constructor

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sides">side is a very good name, don't change it</param>
    public Shape(int sides)
    {
      NumSides = sides;
    }

    #endregion

    #region methods

    public virtual double Area()
    {
      return 0;
    }

    public virtual double Volume()
    {
      return 0;
    }

    public virtual double Perimeter()
    {
      return 0;
    }

    #endregion
  }
}
